#!/usr/bin/env ruby


# ************************* REQUIREMENTS MODULE ************************* #


require './Clases/Gene.rb'
require './Clases/SeedStock.rb'
require './Clases/Cross.rb'


# ************************* PRESENTATION ************************* #


puts "Hi! My name is PILI, and I will guide you through the process of modifying and editing a database as part of the \'Bioinformatics Programming Challenges\' subject at UPM's Masters in Computational Biology"

puts "First, let me check the input is correct..."


# ************************* IMPUT CHECK MODULE ************************* #


#Have I provided 4 arguments?
if ARGV.length() != 4
    abort("[ERROR]: Wrong input format. I expected \'ruby process_database.rb  gene_information.tsv  seed_stock_data.tsv  cross_data.tsv  new_stock_file.tsv\'")
else
    #Does the new_stock_file already exist? I dont want to overrite it
    ARGV.each_with_index() do |file_argv, index|
        if index == 3
            if File.file?(file_argv) == true
                printf "[WARNING]: The specified output file: #{file_argv} already exists. Do you wish to delete it? [y/N]: "
                prompt = STDIN.gets.chomp
                if ( prompt == "y" or prompt == "Y" or prompt == "Yes" or prompt == "yes" )
                    File.delete(file_argv)
                else
                    #If still no valid file is provided, abort
                    abort("[ERROR]: The specified output file: #{file_argv} already exists. Please, specify another one.")
                end
            else
                puts "Seems like everything is OK!"
            end
        elsif File.file?(file_argv) == false #If a file does not exist, set warning
            abort("[ERROR]: The specified file: #{file_argv} does not exist. Please check the provided input")
        end
    end

end

puts


## ************************* PLANTING MODULE ************************* #


puts "Now, I am going to plant some seeds..."
puts "Planting takes some time..."
sleep 1

#First, I must load the Gene file, so that the gene objects are created for the SeedStock database
all_genes = Gene.load_from_file(ARGV[0])

#Then, I load the seedstock file and create objects accordingly
all_seedstock = SeedStock.load_from_file(ARGV[1])

#For each seedstock object, plant 7 seeds and see what happens
all_seedstock.each do |this_seed|
    this_seed[1].plant(7) #this_seed[1] extracts the this_seed object from the hash
end

#Re-generate database
SeedStock.write_database(ARGV[3])
puts


## *************************  CORRELATION MODULE ************************* #

puts "Let me now analyze whether genes are correlated..."
puts "Deploying super smart scientists..."
sleep 1
puts "Cultivating plants..."
sleep 1
puts "Phew, that was fast! I think I got your results!"
puts


#First, I load the crosses table and generate hashes of objects from it
all_crosses = Cross.load_from_file(ARGV[2])

#For each cross object, I analyze linkage
all_crosses.each do |this_cross|
    Cross.analyze_linkage(this_cross[1])
end


## *************************  CORRELATION MODULE ************************* #


puts
puts "And now, for our Final Report:"
sleep 1
puts


#This could have been done in the SeedStock class, but I thought adding it here showed that I understood how my objects work
#I dont quite get why this piece of code ignores empty @linked_genes arrays; should investigate further
all_genes.each do |this_gene|
    this_gene[1].linked_genes.each do |linked| #Nice! Ruby's each "ignores" the empty arrays, so I dont get any pesky empty lines
        linked_gene, chi_squared = linked #Remember: I saved this as an array
        puts "#{this_gene[1].gene_name} is linked to #{linked_gene}"
    end
end

#And all of this without using a single for! :D
#I do know that I have used some CamelCase variables when I shouldn't; I have tried to fix most of them on V2, but some remain


