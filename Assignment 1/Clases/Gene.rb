#!/usr/bin/env ruby

require 'csv'

class Gene
    attr_accessor :gene_id
    attr_accessor :gene_name
    attr_accessor :mutant_phenotype
    attr_accessor :linked_genes

    @@all_genes = {}

    def initialize(params = {})
        #Variable assignments have to go here in case I need to print them in the abort message
        @gene_id = params.fetch(:gene_id, nil)
        @gene_name = params.fetch(:gene_name, nil)
        @mutant_phenotype = params.fetch(:mutant_phenotype, nil)

        @linked_genes = {} #Initialize hash (also: Hash.New)

        format_gene = /A[Tt]\d[Gg]\d\d\d\d\d/ #Regex for the correct AGI locus format
        unless format_gene.match(@gene_id) #Could also do an If to check if nil and indicate
            abort("[ERROR]: Wrong ID format found for ID: #{@gene_id} in file: #{ARGV[0]}. Please, fix the ID format to match #{format_gene} and come back later")
        end
    end

    def self.find_gene_by_id(id) #Find gene by ID
        @@all_genes.each do |gene|
            return gene[1] if gene[1].gene_id == id
        end
    end

    def self.load_from_file(gene_table_file)
        gene_table = CSV.read(gene_table_file, headers: true, col_sep: "\t") #Read TSV as table and generate objects
        gene_table.each() do |row|
            @@all_genes[row["Gene_ID"]] =  Gene.new(:gene_id => row["Gene_ID"], :gene_name => row["Gene_name"], :mutant_phenotype => row["mutant_phenotype"])
        end
        return @@all_genes #Return generated objects
    end

    def add_linked_gene(gene, chi_squared) #Easily add linked genes to the linked_genes property
        @linked_genes[gene] = chi_squared
    end

end
