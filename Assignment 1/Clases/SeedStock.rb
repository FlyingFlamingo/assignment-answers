#!/usr/bin/env ruby

require 'date'

class SeedStock

    attr_accessor :seed_stock
    attr_accessor :gene_id
    attr_accessor :gene
    attr_accessor :last_planted
    attr_accessor :storage
    attr_accessor :grams_remaining

    @@all_seedstock = {}

    def initialize(params = {})
        @gene_id = params.fetch(:gene_id, nil)
        @gene = Gene.find_gene_by_id(@gene_id)
        unless @gene.is_a?(Gene) #The gene attribute must be a gene object for the script to work
            abort("[ERROR:] #{@gene_id} not found in file: #{ARGV[0]}. Cannot match gene_id to gene object.")
        end
        @storage = params.fetch(:storage, nil)
        @seed_stock = params.fetch(:seed_stock, nil)
        @last_planted = params.fetch(:last_planted, nil)
        @grams_remaining = params.fetch(:grams_remaining, nil).to_i #And the remaining grams is, of course, a numeric value

    end


    def self.load_from_file(seedstock_file)
        stock_table = CSV.read(seedstock_file, headers: true, col_sep: "\t") #Read tsv file
        @stock_header = stock_table.headers #Save the headers for later! (headers:true removes them but we need them to write new file)
        stock_table.each.with_index() do |row|
            @@all_seedstock[row["Seed_Stock"]] =  SeedStock.new(:gene_id => row["Mutant_Gene_ID"], :storage => row["Storage"], :seed_stock => row["Seed_Stock"],
                                                                :last_planted => row["Last_Planted"], :grams_remaining => row["Grams_Remaining"])
        end
        return @@all_seedstock #Hash with all the seedstock
    end

    def self.get_seed_stock(stock_ID) #Find a specific seedstock
        @@all_seedstock.each do |stock|
            #stock[1] extracts object from hash
            return stock[1] if stock[1].seed_stock == stock_ID
        end
    end

    def plant(number_of_grams) #Plant function, ONLY CALLABLE AS AN OBJECT "FUNCTION", not as a class function (no "self")
        if @grams_remaining < number_of_grams
            puts "[WARINING]: #{@seed_stock} only had #{@grams_remaining} grams of seed; thus, I could not plant 7 grams as specified. Please, refill with new seed ASAP!"
            @grams_remaining = 0
        elsif @grams_remaining == number_of_grams
            puts "[WARINING]: #{@seed_stock}'s seedstock has been depleted. Please, refill with new seed ASAP! "
            @grams_remaining = 0 #If there are 7 grams there'll be 0 left
        else
            @grams_remaining = @grams_remaining - 7
        end
        @last_planted = DateTime.now.strftime('%-d/%-m/%Y') #Update date to today using the "Date" module
    end

    def self.write_database(new_stock_file)
        #This could also be done by recreating the seedstock_file as a table and then using CSV.open to save it, but I felt like this was more efficient
        new_table = File.open(new_stock_file, 'w')
        new_table.puts(@stock_header.join("\t")) #So that the headers preserve the tsv format, and are not ptinted line-by-line
        @@all_seedstock.each do |this_stock|
            #As @@all_seedstock is a hash of all the objects with an object name as its key, this_stock[1] is the object propper
            new_table.puts([this_stock[1].seed_stock, this_stock[1].gene_id, this_stock[1].last_planted, this_stock[1].storage, this_stock[1].grams_remaining].join("\t"))
        end
    end

end
