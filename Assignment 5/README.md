## Assignment 5

Using Jupyter Notebook and [the SparkQL kernel](https://github.com/paulovn/sparql-kernel), please answer the following questions:

* How many protein records are in UniProt?
* How many Arabidopsis thaliana protein records are in UniProt?
* Retrieve pictures of Arabidopsis thaliana from UniProt
* What is the description of the enzyme activity of UniProt Protein Q9SZZ8
* Retrieve the protein ids, and date of submission, for proteins that have been added to UniProt this year
* How  many species are in the UniProt taxonomy?
* How many species have at least one protein record?
* Find the AGI codes and gene names for all Arabidopsis thaliana  proteins that have a protein function annotation description that mentions “pattern formation”
* Find the MetaNetX Reaction identifier (starts with “mnxr”) for the UniProt Protein uniprotkb:Q18A79
* What is the “mnemonic” Gene ID and the MetaNetX Reaction identifier for the protein that has “Starch synthase” catalytic activity in Clostridium difficile (taxon 272563)?

As a deliverable, please generate a Jupyter Notebook **with all the answers visible**
