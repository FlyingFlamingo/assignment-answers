#!/usr/bin/env ruby

# ************************* REQUIREMENTS MODULE ************************* #


require './Clases/ImportExportModule.rb'
require './Clases/BlastCmdEnhanced.rb'
require 'bio'
require 'set'


# ************************* PRESENTATION ************************* #


#Some computer talking...
puts "Hi! My name is PILI, your pluri-employed, not-really-paid assistant for BioInformatics tasks!"
puts "You may already know me from other assignments as part of the \'Bioinformatics Programming Challenges\' subject at UPM's Masters in Computational Biology. Its nice to see you back!"
puts "Today, I have the task of finding putative orthologues of Protein #{ARGV[1]} on Genome #{ARGV[0]}!"


# ************************* IMPUT CHECK MODULE ************************* #


puts "First, let me check the input..."
ImportExportModule.Check_input(ARGV) #Checks that files exists and that they follow fasta format


# ************************* BLASTING MODULE ************************* #


puts
puts "Now, let me create a blast DB, using your target sequence, #{ARGV[0]}..."
targetdatabase = "./Databases/#{File.basename(ARGV[0]).split('.')[0]}_db"
BlastCmdEnhanced.makeblastdb(ARGV[0], targetdatabase)
puts "Database created successfully !"
puts "BLASTing each protein in #{ARGV[1]} against said database and getting the best hits..."
blast_results = BlastCmdEnhanced.makeblast_besthits('tblastn', targetdatabase, ARGV[1], 0) # 0 deactivates identity
puts "#{blast_results.length} results found"


puts
puts "Then, we parse #{ARGV[0]} to get the sequence of the results"
fakefile = ""
Bio::FlatFile.auto(ARGV[0]).each_entry do |entry|
    if blast_results.values.uniq.include?(entry.entry_id)
        fakefile = "#{fakefile}\n#{entry}"
    end
end

#Creating a temp file might seem like a suboptimal option, but, given the long processing
#time (7+ hours), any potential time savings should be considered, and Bio::FlatFile.auto()
#doesnt seem really able to work just with the "fakefile" string. So, it has to be like this!
File.open("fakefile.tmp", 'w+') { |report| report.puts(fakefile) }


# ************************* RECIPROCAL-BLASTING MODULE ************************* #


puts
puts "Now, let me do a reciprocal BLAST of these hits against the original #{ARGV[1]}"
puts "First, we build the database..."
querydatabase = "./Databases/#{File.basename(ARGV[1]).split('.')[0]}_db"
BlastCmdEnhanced.makeblastdb(ARGV[1], querydatabase)

puts "And now, we do the reciprocal best BLAST!"
reciprocal_blast_results = BlastCmdEnhanced.makeblast_besthits('blastx', querydatabase, 'fakefile.tmp', 0) # 0 deactivates identity
puts "#{reciprocal_blast_results.length} results found"
File.delete('fakefile.tmp')


# ************************* ORTHOLOGY MODULE ************************* #


puts "Now, let me check for orthologues..."
reciprocal_tolist = reciprocal_blast_results.keys.zip(reciprocal_blast_results.values)
blast_tolist = blast_results.keys.zip(blast_results.values)
orthologues = []
reciprocal_tolist.each{|each_reciprocal|  blast_tolist.each{|each_blast| if Set.new(each_reciprocal) == Set.new(each_blast) then orthologues << each_blast.sort end}}
orthologues.uniq! #Do unique on-place

puts "#{orthologues.length} orthologues found!"

# ************************* REPORT-WRITING MODULE ************************* #


puts
puts "I will now proceed to write a report with the orthologues names"
ImportExportModule.Write_report(orthologues.uniq, ARGV[2])


#Computer talk - I thought this would be funny
puts "It was such a blast getting to know you!"
puts "I'm having a great time talking to you, I don't feel like hanging up"
puts "I'd rather you hang up :p"
puts "Hang up? [y/N]"
prompt = STDIN.gets.chomp
while prompt  != "y"  do
    puts "No, you hang up!"
    puts "Hang up? [y/N]"
    prompt = STDIN.gets.chomp
end

puts "See you on Assignment 5!"
