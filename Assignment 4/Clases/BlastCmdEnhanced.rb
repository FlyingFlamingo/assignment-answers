#!/usr/bin/env ruby

require 'progress_bar'
require 'set'

# @author Pablo Marcos
# A class that makes BLAST easier to use on the command line, and even finds putatively orthologous genes! :p
# @note This does not define an object "per-se", but rather is just a compilation of methods
class BlastCmdEnhanced

    # Guesses the type of sequence present inside a fasta file
    # @param [String] file a file with FASTA format
    # @return [String] the type of sequence
    def self.guess_fastatype(file)
        first_sequence = Bio::Sequence.new(Bio::FlatFile.auto(file).next_entry.seq)
        if first_sequence.guess == Bio::Sequence::NA # if that sequence is type NA
            return 'nucl'
        else
            return 'prot'
        end
    end

    # Guesses the type of sequence present inside a fasta file
    # @param [String] genome The source file for the BLAST database
    # @param [String] path_to_db The path where the database will be saved
    # @return [None] the blast database
    def self.makeblastdb(genome, path_to_db = './Blast/blastdb')
        if Dir.exist?(File.dirname(path_to_db)) == false then Dir.mkdir File.dirname(path_to_db) end
        if Dir.glob("#{path_to_db}*").any?
            printf "[WARNING]: The specified name: #{path_to_db} is already in use or will be used by the program. Continuing might cause data loss. Proceed? [y/N]: "
            prompt = STDIN.gets.chomp
            if ( prompt == "y" or prompt == "Y" or prompt == "Yes" or prompt == "yes" )
                Dir.glob("#{path_to_db}*").each {|x| File.delete(x)}
            else
                abort("[ERROR]: The specified output file: #{path_to_db} already exists. Please, specify another one.")
            end
        end
        system("makeblastdb -in #{genome} -dbtype #{self.guess_fastatype(genome)} -out #{path_to_db} > /dev/null")
    end

    # Makes BLAST and takes the best hit
    # @param [String] type the blast type: blastp, blastx, blastn, tblastx or tblastn
    # @param [String] database The BLAST database; can be generated with self.makeblastdb()
    # @param [String] query The query file for BLAST. MUST be a file
    # @param [Float] e_value The e-value for BLAST; only queries with an e_value smaller or equal to this one will be selected
    # @param [Float] identity_treshold The identity treshold for our BLAST search
    # @param [Float] coveraqe_treshold The coverage treshold for our BLAST search
    # @return [Hash] a hash, where each query is the key and the best hits are the values
    # @note This time, I added the progressbar here, inside the module, instead of in the main script; I thought it made more sense this way
    # @note I did this function instead of two separate makeblast and besthits functions, since I felt those two separate functions would be a bit useless and add nothing on top of the bio package
    def self.makeblast_besthits(type, database, query, identity_treshold = 30, coveraqe_treshold = 50, e_value = 1e-5)
        best_hits, bar_length = {}, 0
        Bio::FlatFile.auto(query).each {|x| bar_length +=1 }
        bar = ProgressBar.new(bar_length, :bar, :percentage, :elapsed)
        local_factory = Bio::Blast.local("#{type}","#{database}"," -F 'm S' ")
        Bio::FlatFile.auto(query).each_entry do |each_query|
            report = local_factory.query(each_query)
            if report.hits[0] and report.hits[0].evalue and report.hits[0].identity and report.hits[0].overlap and report.hits[0].query_len
                identity_percentage = ( report.hits[0].identity.to_f / report.hits[0].overlap.to_f ) * 100
                coveraqe_percentage = ( ( report.hits[0].query_end.to_f - report.hits[0].query_start.to_f ) / report.hits[0].query_len.to_f ) * 100
                if report.hits[0].evalue <= e_value and identity_percentage >= identity_treshold and coveraqe_percentage >= coveraqe_treshold
                    best_hits[each_query.entry_id] = report.hits[0].definition.split("|")[0].strip
                end
            end
            bar.increment!
        end
        return best_hits
    end

    # Finds putative orthologues between two fasta files by doing "reciprocal best BLAST"
    # @param [String] query query for the first BLAST (target for the second). Must be filename such that ./Path/../query.fa
    # @param [String] target target for the first BLAST (query for the second). Must be filename such that ./Path/../target.fa
    # @param [Float] e_value The e-value for BLAST; only queries with an e_value smaller or equal to this one will be selected
    # @param [Float] identity_treshold The identity treshold for our BLAST search
    # @param [Float] coveraqe_treshold The coverage treshold for our BLAST search
    # @return [Array] orthologues an array of the found putatively orthologous genes
    # @note This function is basically the assignment itself. I have not used it since I wanted to add more text, but it makes everything more reusable
    def self.find_orthologues(target, query, identity_treshold = 30, coveraqe_treshold = 50, e_value = 1e-5)
        targetdatabase = "./Databases/#{File.basename(target).split('.')[0]}_db"
        self.makeblastdb(target, targetdatabase)

        blast_results = self.makeblast_besthits('tblastn', targetdatabase, query, identity_treshold, coveraqe_treshold, e_value)

        querydatabase = "./Databases/#{File.basename(query).split('.')[0]}_db"
        self.makeblastdb(query, querydatabase)

        fakefile = ""
        Bio::FlatFile.auto(ARGV[0]).each_entry do |entry|
            if blast_results.values.uniq.include?(entry.entry_id)
                fakefile = "#{fakefile}\n#{entry}"
            end
        end
        File.open("fakefile.tmp", 'w+') { |report| report.puts(fakefile) }

        reciprocal_blast_results = self.makeblast_besthits('blastx', querydatabase, 'fakefile.tmp', identity_treshold, coveraqe_treshold, e_value)
        File.delete('fakefile.tmp')

        reciprocal_tolist = reciprocal_blast_results.keys.zip(reciprocal_blast_results.values)
        blast_tolist = blast_results.keys.zip(blast_results.values)

        orthologues = []
        reciprocal_tolist.each{|each_reciprocal|  blast_tolist.each{|each_blast| if Set.new(each_reciprocal) == Set.new(each_blast) then orthologues << each_blast.sort end}}
        orthologues.uniq!
        return orthologues
    end

end
