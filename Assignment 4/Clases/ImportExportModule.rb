#!/usr/bin/env ruby

require 'rest-client'

# @author Pablo Marcos
# A class that manages import checking, file loading, && downloading data from the web
# @note This does not define an object "per-se", but rather is just a compilation of methods
# @since Assignment 2
class ImportExportModule

    # Processes the input && checks if its allright
    # @param [Array] argvs an array of all the arguments initially presented in a comm&& line run of the script
    # @return [None] This returns nothing by itself: it just doesn't abort the program, which shows all went smoothly
    # @note This imput check module has been generously provided by Pablo Marcos' Assignment 2
    def self.Check_input(argvs)
        if argvs.length() != 3
            abort("[ERROR]: Wrong input format. I expected \'ruby blast_orthologues.rb  target_genome.fa protein.fa report_file.txt\'")
        else
            #Does the report_file already exist? I dont want to overrite it
            argvs.each_with_index() do |file_argv, index|
                if index == 2
                    #Check for any file with the provided name or any file that the program could create from it
                    if File.file?(file_argv)
                        printf "[WARNING]: The specified name: #{file_argv} is already in use or will be used by the program. Continuing might cause data loss. Proceed? [y/N]: "
                        prompt = STDIN.gets.chomp
                        if ( prompt == "y" or prompt == "Y" or prompt == "Yes" or prompt == "yes" )
                            File.delete(file_argv)
                        else
                            #If still no valid file is provided, abort
                            abort("[ERROR]: The specified output file: #{file_argv} already exists. Please, specify another one.")
                        end
                    else
                        puts "Seems like the input is OK!"
                    end
                elsif File.file?(file_argv) == false #If a file does not exist, set warning
                    abort("[ERROR]: The specified file: #{file_argv} does not exist. Please check the provided input")
                else
                   self.check_itsfasta(file_argv) #If the file exists, check its fasta
                end
            end
        end
    end

    # Check whether a file follows the FASTA format; it does so in a really simple way, it just checks for the fasta header on the first line
    # @param [String] file_argv the file for which we want to check if its fasta
    # return [Bool] true if its fasta; false if not
    # @todo This only checks that there is a fasta header on the first line. Could we expand it to check for full fasta format?
    def self.check_itsfasta(file_argv)
        isfasta = false
        if  file_argv.split('.')[-1] != 'fasta' && file_argv.split('.')[-1] != 'fna' && file_argv.split('.')[-1] != 'ffn' && file_argv.split('.')[-1] != 'frn' && file_argv.split('.')[-1] != 'fa'
            abort("[ERROR]: The specified file: #{file_argv} does not use a fasta extension. Please, use a correct fasta file")
        elsif (File.open("#{file_argv}") {|f| f.readline})[0] != '>'
            abort("[ERROR]: The specified file: #{file_argv} does not follow the fasta format. All sequences must start with '>'")
        else
            isfasta = true
        end
        return isfasta
    end

    # Writes a cute report based on a list of lists of orthologues
    # @param [Array] orthologues an array of arrays where each array has a pair of values: the orthologous genes
    # @param [String] filename the filename for the report
    # return [File] it returns nothing, just writes the report
    def self.Write_report(orthologues, filename)
        File.open(filename, 'w+') do |report|
            report.puts("################################################################################")
            report.puts("\n\n")
            report.puts("                       Report on Putative Orthologous Genes                      ")
            report.puts("")
            report.puts("                         by Pablo Ignacio Marcos López                          ")
            report.puts("\n\n")
            report.puts("################################################################################")
            report.puts("")
            report.puts("I found #{orthologues.length} genes whith putative ortologues")
            report.puts("")
            report.puts("Said pairs of orthologues were:")
            report.puts("")
            orthologues.each do |each_array|
                report.puts("\t * #{each_array.join(" - ")}")
            end
            report.puts("")
            report.puts("")
            report.puts("")
            report.puts("")
            report.puts("Report generated the #{Time.now}")
        end
    end

end
