#!/usr/bin/env ruby

require 'bio'

# @author Pablo Marcos
# A class that reads EMBL-formatted data and is able to write GFF3 files from it
# @note This, per se, is not needed: BioRuby already has its own object types, and this code could all have been written in the main script; but, it looks cleaner and more reusable this way
class Embl_to_GFF3

    #A Hash of all BioRuby objects
    @@all_bioseqs = {}

    #As explained, this is not really needed; I am leaving it as an empty note! :p
    def initialize(params = {})
    end

    # Gets the positions where a given pattern occurs for the exons a given gene
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @param [String] mutation_site a DNA sequence which will be searched for
    # @return [Array] a simplified array of the matches' positions, with BioRuby format (a..b). First element is sense strand and second element is antisense strand
    # @todo I have found the Bio::Fetch::EBI.query('ensemblgenomesgene','locus_code') module, which could automate things. Should be searched further, it gave errors when trying to implement
    def self.Get_positions_inexons(locus_code, mutation_site)
        position_sense, position_antisense = [], []
        mutation_site_bio = Bio::Sequence.auto(mutation_site)
        search_sense = mutation_site_bio.to_re
        search_antisense = mutation_site_bio.reverse_complement.to_re
        response = ImportExportModule.fetch("http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=ensemblgenomesgene&format=embl&id=#{locus_code}")
        embl = Bio::EMBL.new(response)
        bio_seq = embl.to_biosequence
        @@all_bioseqs["#{locus_code}"] = bio_seq
        embl.features.each do |feature|
            if feature.feature == "exon"
                feature.locations.each do |location|
                    exon_seq=embl.seq[location.from..location.to]
                    if location.strand == 1 && exon_seq
                        (exon_seq.enum_for(:scan, search_sense).map { Regexp.last_match.begin(0) }).each do |search_start|
                            position_sense << "#{search_start+1}..#{search_start+mutation_site.length}" #The +1 here, and the -1 below, are to adjust for bioruby's weird af indexing system
                        end
                    elsif location.strand == -1 && exon_seq
                        (search_start = exon_seq.enum_for(:scan, search_sense).map { Regexp.last_match.begin(0) }).each do |search_start|
                            position_antisense << "complement(#{search_start}..#{search_start+mutation_site.length-1})" #And to adjust for the fact that the antisense strand has initial misalignment
                        end
                    end
                end
            end
        end
        return [position_sense.uniq, position_antisense.uniq]
    end

    # Adds a mutation site in exon feature to an existing BioRuby object. If no object exists, it creates one
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @param [Array] positions a simplified array of the matches' positions, with BioRuby format (a..b). First element should be sense strand and second element should be antisense strand
    # @return [Object] a Bio::Feature object, annotated with the specified mutation site in the given positions
    # @todo: [According to rubydoc](https://rubydoc.info/gems/bio/1.5.2/Bio/Features), Bio::Feature will soon be deprecated. It should thus be replaced
    # @note Being able to generate the Bio::EMBL object from 0 makes it more general
    # @todo: We had to use Bio::Feature, since Bio::EMBL cannot be so easily modified (it represents a database entry!)
    def self.Add_features(locus_code, positions, motif)
        bio_seq = self.find_by_id(locus_code)
        if defined?(bio_seq).nil? then bio_seq = Bio::EMBL.new(ImportExportModule.fetch("http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=ensemblgenomesgene&format=embl&id=#{locus_code}")).to_biosequence end
        positions.each_with_index do |all_sites, index|
            all_sites.each do |site|
                if index == 0 then strand = '+' else strand = '-' end
                features_to_add = Bio::Feature.new('mutation_site_bioinfo', site)
                features_to_add.append(Bio::Feature::Qualifier.new('mutation_motif',motif))
                features_to_add.append(Bio::Feature::Qualifier.new('function','insertion site'))
                features_to_add.append(Bio::Feature::Qualifier.new('note', 'found by PILI'))
                features_to_add.append(Bio::Feature::Qualifier.new('strand', strand))
                bio_seq.features << features_to_add
            end
        end
        return bio_seq
    end

    # Writes a 'simpler' GFF3 file, where the coordinates of the features are relative to the small pieces of DNA retrieved from EMBL
    # @param [String] filename the name of the file wich we will create. A trailing '_relative.gff3' will be added to it
    # @param [Hash] bioseq_objects a hash of all bioseq objects, with locus codes as key and Embl_to_GFF3.rb objects as values
    # @param [String] source name of the program that generated this feature, or the data source (database or project name)
    # @param [String] type type of feature. Must be a term or accession from the SOFA sequence ontology
    # @param [Float] score an accuracy score for the match
    # @param [Integer] phase One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..
    # @return [File] a 'simpler' GFF3 file, where the coordinates of the features are relative to the small pieces of DNA retrieved from EMBL
    # @note Type must be of [standarized SOFA format](https://genomebiology.biomedcentral.com/articles/10.1186/gb-2005-6-5-r44).
    # @note By using bioseq_objects instead of directly @@all_bioseqs, I add a little extra work, but make the function more general.
    # @note See more on the formating on guidelines on [GFF's](http://gmod.org/wiki/GFF3) and [ENSEMBL's](https://m.ensembl.org/info/website/upload/gff3.html) websites
    def self.Write_GFF3_relative(filename, bioseq_objects, source=".",type=".",score=".",phase=".") #PERMITIR QUE ACABE EL REPORT EN NADA O EN GFF3
        File.open("#{filename.split('.')[0]}_relative.gff3", 'w+') do |file|
            file.puts("##gff-version 3")
            bioseq_objects.each do |each_bioseq|
                each_bioseq[1].features.each_with_index do |feature, index|
                    if feature.feature == 'mutation_site_bioinfo' #The features added using self.Add_features! :D
                        chromosome_code = each_bioseq[1].primary_accession.split(":")[2] .to_i
                        position = feature.locations.first
                        strand = feature.assoc['strand']
                        attributes = "ID=#{each_bioseq[0]};Species=#{each_bioseq[1].species.delete(' ')};Repeat=#{feature.assoc['mutation_motif']}" #Some interesting attributes
                        file.puts("chr#{chromosome_code}\t#{source}\t#{type}\t#{position.from}\t#{position.to}\t#{score}\t#{strand}\t#{phase}\t#{attributes}")
                    end
                end
            end
        end
    end

    # Writes a 'complex' GFF3 file; this time, the coordinates will be absolute, i.e. with regards to the *whole chromosome*
    # @param [String] filename the name of the file wich we will create. A trailing '_relative.gff3' will be added to it
    # @param [Hash] bioseq_objects a hash of all bioseq objects, with locus codes as key and Embl_to_GFF3.rb objects as values
    # @param [String] source name of the program that generated this feature, or the data source (database or project name)
    # @param [String] type type of feature. Must be a term or accession from the SOFA sequence ontology
    # @param [Float] score an accuracy score for the match
    # @param [Integer] phase One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..
    # @return [File] a 'complex' GFF3 file, with absolute coordinates with regards to the *whole chromosome*
    def self.Write_GFF3_absolute(filename, bioseq_objects, source=".",type=".",score=".",phase=".")
        File.open("#{filename.split('.')[0]}_absolute.gff3", 'w+') do |file|
            file.puts("##gff-version 3")
            bioseq_objects.each do |each_bioseq|
                each_bioseq[1].features.each_with_index do |feature, index|
                    if feature.feature == 'mutation_site_bioinfo'
                        bioseq_start = each_bioseq[1].primary_accession.split(":")[3].to_i
                        chromosome_code = each_bioseq[1].primary_accession.split(":")[2] .to_i
                        position = feature.locations.first
                        strand = feature.assoc['strand']
                        attributes = "ID=#{each_bioseq[0]};Species=#{each_bioseq[1].species.delete(' ')};Repeat=#{feature.assoc['mutation_motif']}"
                        file.puts("chr#{chromosome_code}\t#{source}\t#{type}\t#{bioseq_start+position.from}\t#{bioseq_start+position.to}\t#{score}\t#{strand}\t#{phase}\t#{attributes}")
                    end
                end
            end
        end
    end

    # Writes a report of which genes do not contain a given feature
    # @param [String] filename the name of the file wich we will create. A trailing '_relative.gff3' will be added to it
    # @param [Hash] bioseq_objects a hash of all bioseq objects, with locus codes as key and Embl_to_GFF3.rb objects as values
    # @param [String] feature The feature name for which we want to search
    # @return [File] a file which shows which genes had instances of feature 'feature'
    # @since: Ruby 2.6 is introducing Array.difference def self.Write_comparative(filename, bioseq_objects, feature)
    def self.Write_comparative(filename, bioseq_objects, feature)
        all_genes = []
        bioseq_objects.each do |each_bioseq|
            each_bioseq[1].features.each_with_index do |feature, index|
                if feature.feature == 'mutation_site_bioinfo'
                    all_genes.append(each_bioseq[0])
                end
            end
        end

        nomatches = bioseq_objects.keys.difference(all_genes)

        File.open("#{filename.split('.')[0]}_nomatches.txt", 'w+') do |report|
            report.puts("################################################################################")
            report.puts("\n\n")
            report.puts("                   Report on Mutagenesis Sites on A.Thaliana                    ")
            report.puts("")
            report.puts("                         by Pablo Ignacio Marcos López                          ")
            report.puts("\n\n")
            report.puts("################################################################################")
            report.puts("")
            report.puts("In the provided file, I found #{nomatches.length} genes which had no appearances")
            report.puts("of the '#{feature}' feature, which corresponds to the pattern CTTCTT.")
            report.puts("")
            report.puts("Said genes were:")
            nomatches.each do |gene|
                report.puts("\t * #{gene}")
            end
            report.puts("")
            report.puts("")
            report.puts("")
            report.puts("")
            report.puts("Report generated the #{Time.now}")
        end
    end

    # Returns a Hash of all Embl_to_GFF3.rb objects
    # @return [Hash] Hash of all bioseq objects, with locus codes as key and bioseq objects as values
    def self.all_bioseqs()
        return @@all_bioseqs
    end

    # Finds a bioseq object by it's locus_code
    # @param [String] locus_code A locus_code for a given gene
    # @return [Object] Returns a given Embl_to_GFF3 object
    def self.find_by_id(locus_code)
        return @@all_bioseqs["#{locus_code}"]
    end

end
