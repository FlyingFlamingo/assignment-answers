#!/usr/bin/env ruby

require 'rest-client'

# @author Pablo Marcos
# A class that manages import checking, file loading, and downloading data from the web
# @note This does not define an object "per-se", but rather is just a compilation of methods
# @since Assignment 2
class ImportExportModule

    # Processes the input and checks if its allright
    # @param [Array] argvs an array of all the arguments initially presented in a command line run of the script
    # @return [None] This returns nothing by itself: it just doesn't abort the program, which shows all went smoothly
    # @note This imput check module has been generously provided by Pablo Marcos' Assignments 1 and 2
    def self.Check_input(argvs)
        if argvs.length() != 2
            abort("[ERROR]: Wrong input format. I expected \'ruby insertional_mutagenesis.rb  SubNetwork_GeneList.txt report_file.gff3\'")
        else
            #Does the report_file already exist? I dont want to overrite it
            argvs.each_with_index() do |file_argv, index|
                if index == 1
                    #Check for any file with the provided name or any file that the program could create from it
                    if File.file?(file_argv) or File.file?("#{file_argv.split('.')[0]}_relative.gff3") or File.file?("#{file_argv.split('.')[0]}_absolute.gff3")
                        printf "[WARNING]: The specified name: #{file_argv} is already in use or will be used by the program. Continuing might cause data loss. Proceed? [y/N]: "
                        prompt = STDIN.gets.chomp
                        if ( prompt == "y" or prompt == "Y" or prompt == "Yes" or prompt == "yes" )
                            if File.file?(file_argv) then File.delete(file_argv) end
                            if File.file?("#{file_argv.split('.')[0]}_relative.gff3") then File.delete("#{file_argv.split('.')[0]}_relative.gff3") end
                            if File.file?("#{file_argv.split('.')[0]}_absolute.gff3") then File.delete("#{file_argv.split('.')[0]}_absolute.gff3") end
                        else
                            #If still no valid file is provided, abort
                            abort("[ERROR]: The specified output file: #{file_argv} already exists. Please, specify another one.")
                        end
                    else
                        puts "Seems like the input is OK!"
                    end
                elsif File.file?(file_argv) == false #If a file does not exist, set warning
                    abort("[ERROR]: The specified file: #{file_argv} does not exist. Please check the provided input")
                end
            end
        end
    end

    # Reads a list of genes
    # @param [String] file a txt file with one AGI locus code per line
    # @return [Array] all_locus an Array of all the locus codes found in the pseudo-"table" file
    # @note This table loading module has been generously provided by Pablo Marcos' Assignment 2
    def self.load_table(file)
        all_locus = []
        File.foreach(file) do |locus_code|
            locus_code = locus_code.strip().upcase()
            format_gene = /A[Tt]\d[Gg]\d\d\d\d\d/ #Regex for the correct AGI locus format
            unless format_gene.match(locus_code) #Could also do an If to check if nil and indicate
                abort("[ERROR]: Wrong ID format found for ID: #{locus_code} in file: #{file}. Please, fix the ID format to match #{format_gene} and come back later")
            end
            all_locus << locus_code
        end
        return all_locus
    end

    # Makes ruby's Rest Client easier to manage
    # @param [String] url a valid url where the user wishes to fetch data
    # @return [String] response The response given by the Net Client. If an exception is raised, instead of crashing, it prints it on screen: 404s, bad urls... everything is told back to the user! :p
    # @note I was thinking about using Net Client, but it didn't seem as good with error paring; also, this piece of code was already written, and reusing is less work! :p
    def self.fetch(url)
        begin
            response = RestClient.get(url)
            return response
        rescue RestClient::ExceptionWithResponse => e
            puts e.inspect
        rescue RestClient::Exception => e
            puts e.inspect
        rescue Exception => e
            puts e.inspect
        end
    end

end
