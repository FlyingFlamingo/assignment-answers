#!/usr/bin/env ruby

# ************************* REQUIREMENTS MODULE ************************* #


require './Clases/ImportExportModule.rb'
require './Clases/Embl_to_GFF3.rb'
require 'progress_bar'
require 'bio'


# ************************* PRESENTATION ************************* #


#Some computer talking...
puts "Hi! My name is PILI, your pluri-employed, not-really-paid assistant for BioInformatics tasks!"
puts "You may already know me from other assignments as part of the \'Bioinformatics Programming Challenges\' subject at UPM's Masters in Computational Biology. Its nice to see you back!"
puts "Today, I have the task of finding mutageneis sites, to the fashion of CTTCTT, in whatever genes you wish!"


# ************************* IMPUT CHECK MODULE ************************* #


puts "First, let me check the input..."
ImportExportModule.Check_input(ARGV)


# ************************* MUTAGENESIS-FINDING MODULE ************************* #


puts
puts "Importing Genes and checking for mutagenesis sites. Please, be patient..."
all_genes = ImportExportModule.load_table(ARGV[0]) #Load the (presumably existing) SubNetwork_GeneList.txt file

bar = ProgressBar.new(all_genes.length, :bar, :percentage, :elapsed) #Cool! A progress bar
all_genes.each() do |locus_code|
    positions = Embl_to_GFF3.Get_positions_inexons(locus_code, 'CTTCTT') #Get the mutated positions for each gene
    Embl_to_GFF3.Add_features(locus_code, positions, 'CTTCTT') #Add them to a Bio::Feature object
    bar.increment! #Tell the bar to advance
end


# ************************* REPORT-WRITING MODULE ************************* #


bioseq_objects = Embl_to_GFF3.all_bioseqs() #First, get all the bioseq objects

puts
puts "Mutagenesis sites found successfully! Now, let me write a report with my findings"
puts "First, I will write a 'simpler' GFF3 file, where the coordinates of the features are relative to the small pieces of DNA retrieved from EMBL"
puts "Writing..."
sleep 1 #Dramatic effect
Embl_to_GFF3.Write_GFF3_relative(ARGV[1], bioseq_objects, 'PILI', 'exon_region')

puts
puts "Done! Now, let me create another GFF3 file; this time, the coordinates will be 'absolute', i.e. with regards to the *whole chromosome*!"
puts "Writing..."
sleep 2 #Even moar drama for this more complex task
Embl_to_GFF3.Write_GFF3_absolute(ARGV[1], bioseq_objects, 'PILI', 'exon_region')

puts
puts "Finally, let me tell you which genes had no CTTCTT repetitions in any of their exons"
puts "Thats an easy one"
sleep 0.5 #Much less drama
Embl_to_GFF3.Write_comparative(ARGV[1], bioseq_objects, 'mutation_site_bioinfo')

#More computer talk...
puts
puts "The script has correctly finalized! Now, it is recommended that you upload the results to Ensembl to check they are ok!"
puts "Its is recommended that you overlay it atop AT2G46340 http://plants.ensembl.org/info/website/upload/index.html"

puts
puts "See you back for assignment 3!"
puts "Byeee!"
