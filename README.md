# assignment-answers

A git repo to upload the answers for the "Bioinformatics Programming Challenges" subject at UPM's Masters in Computational Biology

---

## Asignment 1

Your task is to use Object-oriented programming to achieve two things:

1) "Simulate" planting 7 grams of seeds from each of the records in the seed stock genebank; then, you should update the genebank information to show the new quantity of seeds that remain after a planting. The new state of the genebank should be printed to a new file, using exactly the same format as the original file seed_stock_data.tsv. If the amount of seed is reduced to zero or less than zero, then a friendly warning message should appear on the screen. The amount of seed left in the gene bank is, of course, not LESS than zero guiño


2) Process the information in cross_data.tsv and determine which genes are genetically-linked. To achieve this, you will have to do a Chi-square test on the F2 cross data. If you discover genes that are linked, this information should be added as a property of each of the genes (they are both linked to each other).

To run the script, use: ```ruby process_database.rb  ./SampleData/gene_information.tsv  ./SampleData/seed_stock_data.tsv ./SampleData/cross_data.tsv  new_stock_file.tsv```

## Asignment 2

A recent paper executes a meta-analysis of a few thousand published co-expressed gene sets from Arabidopsis.  They break these co-expression sets into ~20 sub-networks of <200 genes each, that they find consistently co-expressed with one another.  Assume that you want to take the next step in their analysis, and see if there is already information linking these predicted sub-sets into known regulatory networks.  One step in this analysis would be to determine if the co-expressed genes are known to bind to one another.

Using the co-expressed gene list from the "Assignment 2" folder:
* Use a combination of any or all of:  dbFetch, Togo REST API, EBI’s PSICQUIC REST API, DDBJ KEGG REST, and/or the Gene Ontology
* Find all protein-protein interaction networks that involve members of that gene list
* Determine which members of the gene list interact with each other.

To run the script, use: ```ruby find_interactions.rb  ./SampleData/ArabidopsisSubNetwork_GeneList.txt report_file.txt```

## Assignment 3

#### Tasks:  for 10% (easy)

1. **Using BioRuby**, examine the sequences of the ~167 Arabidopsis genes from the last assignment by retrieving them from whatever database you wish
2. Loop over every exon feature, and scan it for the CTTCTT sequence
3. Take the coordinates of every CTTCTT sequence and create a new Sequence Feature. Add that new Feature to the EnsEMBL Sequence object.
4. Once you have found and added them all, loop over each one of your CTTCTT features and create a GFF3-formatted file of these features.
5. Output a report showing which genes on your list do NOT have exons with the CTTCTT repeat

#### Tasks:  for 10% (hard)

6. Re-execute your GFF file creation so that the CTTCTT regions are now in the full chromosome coordinates used by EnsEMBL.  Save this as a separate **new** file.
7. Prove that your GFF file is correct by uploading it to ENSEMBL and adding it as a new “track” [to the genome browser of Arabidopsis](http://plants.ensembl.org/info/website/upload/index.html)
8. Along with your code, for this assignment please submit a screenshot of your GFF track for the AT2G46340 gene on the ENSEMBL website to proof that you were successful.

To run the script, use: ```ruby insertional_mutagenesis.rb  ./SampleData/ArabidopsisSubNetwork_GeneList.txt report_file.gff3```

## Assignment 4

Using BioRuby to do BLAST and parse the BLAST-generated reports, find the orthologue pairs between species Arabidopsis and S. pombe. You can use "reciprocal best BLAST": scan for Protein A of Species A in the whole proteome of Species B, and then BLAST the top hit against all proteins in Species A. If the top hits match, the protein is a good Orthologue candidate.

To decide on "sensible" BLAST parameters, do a bit of online reading, and cite the paper or website that provided the information. Also, write a few sentences describing how you would continue to analyze the putative orthologues you just discovered, to prove that they really are orthologues. You DO NOT need to write the code - just describe in words what that code would do.

To run the script, use: ```ruby blast_orthologues.rb  ./SampleData/target_genome.fa ./SampleData/protein.fa report_file.txt```

## Assignment 5

Using Jupyter Notebook and [the SparkQL kernel](https://github.com/paulovn/sparql-kernel), please answer the following questions:

* How many protein records are in UniProt?
* How many Arabidopsis thaliana protein records are in UniProt?
* Retrieve pictures of Arabidopsis thaliana from UniProt
* What is the description of the enzyme activity of UniProt Protein Q9SZZ8
* Retrieve the protein ids, and date of submission, for proteins that have been added to UniProt this year
* How  many species are in the UniProt taxonomy?
* How many species have at least one protein record?
* Find the AGI codes and gene names for all Arabidopsis thaliana  proteins that have a protein function annotation description that mentions “pattern formation”
* Find the MetaNetX Reaction identifier (starts with “mnxr”) for the UniProt Protein uniprotkb:Q18A79
* What is the “mnemonic” Gene ID and the MetaNetX Reaction identifier for the protein that has “Starch synthase” catalytic activity in Clostridium difficile (taxon 272563)?

As a deliverable, please generate a Jupyter Notebook **with all the answers visible**

## Possible improvements and grades

| Assignment # | Description                     | Grade / 20 | Notes                                                                                                                                                        |
|:------------:|---------------------------------|:----------:|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
|       1      | Simulating planting and linkage |     20     | Great work!                                                                                                                                                  |
|       2      | Finding gene networks           |     16     | The networks are not consolidated (i.e. they dont form trees: use a graph library for that) The function is not really recursive                             |
|       3      | Mutation scanning on exons      |     15     | Contains duplicates, although they are label as coming from different transcripts (so OK!) Only the overlapping ones are found: you should find double that! |
|       4      | Finding Orthologues using BLAST |            |                                                                                                                                                              |
|       5      | SPARQL Queries                  |            |                                                                                                                                                              |


Some possible improvements include: using the argvparser gem instead of ImportExportModule.rb to parse the input, doing something similar to pip install -r requirements.txt
