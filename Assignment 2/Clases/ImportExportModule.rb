#!/usr/bin/env ruby

require 'rest-client'

# @author Pablo Marcos
# A class that manages import checking, file loading, downloading data from the web, and file saving
# @note This does not define an object "per-se", but rather is just a compilation of methods
# @since Assignment 2
class ImportExportModule

    # Processes the input and checks if its allright
    # @param [Array] argvs an array of all the arguments initially presented in a command line run of the script
    # @return [None] This returns nothing by itself: it just doesn't abort the program, which shows all went smoothly
    # @note This imput check module has been generously provided by Pablo Marcos' Assignment 1
    def self.Check_input(argvs)
        if argvs.length() != 2
            abort("[ERROR]: Wrong input format. I expected \'ruby find_interactions.rb  SubNetwork_GeneList.txt report_file.txt\'")
        else
            #Does the report_file already exist? I dont want to overrite it
            argvs.each_with_index() do |file_argv, index|
                if index == 1
                    if File.file?(file_argv) == true
                        printf "[WARNING]: The specified output file: #{file_argv} already exists. Do you wish to delete it? [y/N]: "
                        prompt = STDIN.gets.chomp
                        if ( prompt == "y" or prompt == "Y" or prompt == "Yes" or prompt == "yes" )
                            File.delete(file_argv)
                        else
                            #If still no valid file is provided, abort
                            abort("[ERROR]: The specified output file: #{file_argv} already exists. Please, specify another one.")
                        end
                    else
                        puts "Seems like the input is OK!"
                    end
                elsif File.file?(file_argv) == false #If a file does not exist, set warning
                    abort("[ERROR]: The specified file: #{file_argv} does not exist. Please check the provided input")
                end
            end
        end
    end

    # Reads a list of genes
    # @param [String] file a txt file with one AGI locus code per line
    # @return [Array] all_locus an Array of all the locus codes found in the pseudo-"table" file
    # @note This checks if the locus codes use a valid AGI code format; it intentionally aborts if there are invalid AGI codes
    def self.load_table(file)
        all_locus = []
        File.foreach(file) do |locus_code|
            locus_code = locus_code.strip().upcase()
            format_gene = /A[Tt]\d[Gg]\d\d\d\d\d/ #Regex for the correct AGI locus format
            unless format_gene.match(locus_code) #Could also do an If to check if nil and indicate
                abort("[ERROR]: Wrong ID format found for ID: #{locus_code} in file: #{file}. Please, fix the ID format to match #{format_gene} and come back later")
            end
            all_locus << locus_code
        end
        return all_locus
    end

    # Makes ruby's RestClient easier to manage
    # @param [String] url a url where the user wishes to fetch data
    # @return [String] response The response given by the RestClient. If an exception is raised, instead of crashing, it prints it on screen: 404s, bad urls... everything is told back to the user
    def self.fetch(url)
        begin
            response = RestClient.get(url)
            return response
        rescue RestClient::ExceptionWithResponse => e
            puts e.inspect
        rescue RestClient::Exception => e
            puts e.inspect
        rescue Exception => e
            puts e.inspect
        end
    end

    # Decides which kind of report to write, and calls the appropriated function
    # @param [String] file_argv the file name where we want the report written
    # @param [Array] interactions_list an Array of Arrays of all the interactions, presumably made using InteractionNetwork.rb. Please provide as simplified as possible, or use #InteractionNetwork.Simplify_list
    # @note By default, it generates a tsv file with *all* the go and kegg terms in Array format. A compacted txt file can also be generated
    def self.Write_report(file_argv, interactions_list)
        if file_argv.split(".")[1] == "txt"
            puts "TXT output mode selected. The report will be in a human-readable format"
            self.Write_txt_report(file_argv, interactions_list)
        else
            puts "TSV output mode selected by default. The output will be a tab-separated file"
            self.Write_tsv_report(file_argv, interactions_list)
        end
    end

    # Writes a compacted txt report (using uniq to reduce duplicities). Easier for humans, more complicated for machines
    # @param [String] file_argv the file name where we want the report written
    # # @param [Array] interactions_list an Array of Arrays of all the interactions, presumably made using InteractionNetwork.rb.
    def self.Write_txt_report(file_argv, interactions_list)
        puts "Building report file. Please be patient..."
        report = File.open(file_argv, 'w')
        report.puts("################################################################################")
        report.puts("\n\n")
        report.puts("                 Report on Regulatory Subnetworks on A.Thaliana                 ")
        report.puts("")
        report.puts("                         by Pablo Ignacio Marcos López                          ")
        report.puts("\n\n")
        report.puts("################################################################################")
        report.puts("")
        report.puts("In the provided file, I found #{interactions_list.length} networks.")
        interactions_list.each_with_index() do |each_network, index|
            report.puts("\n")
            report.puts("                                  ********                                  ")
            report.puts("\n")
            report.puts("Network ##{index+1} consisted of the following genes:")
            report.puts("\n\t\t\t\t#{each_network.join(" - ")}")
            report.puts("\n")
            all_go_terms = []
            all_kegg_terms = []
            each_network.each() do |gene|
                Annotations.Annotate_Go(gene).each{|go_term| all_go_terms << go_term}
                Annotations.Annotate_Kegg(gene).each{|kegg_term| all_kegg_terms << kegg_term}
            end

            report.puts("This network was associated with the following Gene Ontologies:")
            all_go_terms.uniq.each{|go| report.puts("\t * GO:#{go[0]} - #{go[1]}")}
            report.puts("\n\n")
            report.puts("And it took part in the following processes:")
            all_kegg_terms.uniq.each{|kegg| report.puts("\t * Kegg ID:#{kegg[0]} - #{kegg[1]}")}
       end
       report.puts("\n\n")
       report.puts("Information was abbreviated to be presented in a more human-friendly way")
       report.puts("If you wish to get the whole information, please geneate the CSV report using:")
       report.puts("`ruby find_interactions.rb ArabidopsisSubNetwork_GeneList.txt report_file.csv`")
    end

    # Writes a full csv report (using uniq to reduce duplicities). Easier for humans, more complicated for machines
    # @param [String] file_argv the file name where we want the report written
    # @param [Array] interactions_list an Array of Arrays of all the interactions, presumably made using InteractionNetwork.rb.
    # @note It had to be tsv, since ruby's Arrays are comma-separated
    # @todo Initially, I wanted the code to work in a separator-agnostic way. Maybe we could find a way?
    def self.Write_tsv_report(file_argv, interactions_list)
        puts "Building report file. Please be patient..."
        report = File.open(file_argv, 'w')
        report.puts( ["Gene1","Gene2", "Gene3", "Gene4", "Go", "Kegg"].join("\t") )
        interactions_list.each() do |each_network|
            all_go_terms = []
            all_kegg_terms = []
            each_network.each() do |gene|
                all_go_terms << Annotations.Annotate_Go(gene).uniq
                all_kegg_terms << Annotations.Annotate_Kegg(gene).uniq
            end
            (4-each_network.length).times{each_network << ""}
            report.puts("#{each_network.join("\t")}\t#{all_go_terms}\t#{all_kegg_terms}")
        end
    end

end
