#!/usr/bin/env ruby

require 'set'

# @author Pablo Marcos
# A class that, given a certain gene, gets all of its interactions, up to the fourth level
#It has the ability of finding regulatory networks inside the list of interactors, given a list of genes to search for.
class InteractionNetwork

    #Contains the AGI code for each gene
    # @return [String] a string which includes the correct AGI locus code for a gene
    attr_accessor :gene_id
    #Contains all relations for a gene, up to 4th level deepness
    # @return [Hash] a hash of hashes which includes all relations for a gene, up to 4th level deepness
    attr_accessor :all_relations
    #Contains all associated networks for a given gene
    # @return [Array] an array which includes  all associated regulatory networks for a given gene
    attr_accessor :associated_networks

    #A Hash of all InteractionNetwork.rb objects
    @@all_networks = {}
    #An Array of all found Interactions
    @@all_associated_networks = []
    #The current locus in use; see #Build_network
    @@current_locus
    #The first-level locus in use; see #Build_network
    @@locus_level1

    #A constructor for InteractionNetwork.rb objects
    def initialize(params = {})
        @gene_id = params.fetch(:gene_id, nil)
        @associated_networks = params.fetch(:associated_networks, Array.new)
        @all_relations = params.fetch(:all_relations, Hash.new)
    end

    # Builds a network of interactions for a given gene by recursively searching for interactions, using Gene.rb
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @param [Numeric] number_of_iterations the deepness of interactions wanted for the network; currently hardcoded to 4
    # @todo The function could be made to run recursively; I tried, but the main problem is that I dont know how to populate the hashes
    def self.Build_network(locus_code, number_of_iterations = 4)
        interactors = Gene.find_interactions(locus_code).linked_genes.uniq #Avoid repetitions
        if interactors.empty? then return end

        if number_of_iterations == 4
            @@all_networks["#{locus_code}"] = InteractionNetwork.new(:gene_id => locus_code,  :all_relations => interactors.each_with_object({}).to_h)
            @@current_locus = locus_code
        elsif number_of_iterations == 3
            @@all_networks["#{@@current_locus}"].all_relations["#{locus_code}"] = interactors.each_with_object({}).to_h
            @@locus_level1 = locus_code
        elsif number_of_iterations == 2
              @@all_networks["#{@@current_locus}"].all_relations["#{@@locus_level1}"]["#{locus_code}"] = interactors
             return
        end
        interactors.each {|each_interactor| self.Build_network(each_interactor, number_of_iterations - 1)}
    end

    # Search in a previously existing Hash of Hashes of interactions to build regulatory networks, based on a list
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @param [Array] genes_to_match an array of the genes that are to be linked to form regulatory networks
    # @todo This is currently hardcoded for 4 deepness levels. Could it be generalized?
    def self.Search_in_networks(locus_code, genes_to_match)
        my_networks = []
        genes_to_match.delete("#{locus_code}")
        if  @@all_networks["#{locus_code}"]
            @@all_networks["#{locus_code}"].all_relations.each do |interactors_l2|
                interactors_l2[1].each do |interactors_l3|
                    interactors_l3.each do |interactors_l4|
                        if genes_to_match.include?(interactors_l4)
                            my_networks << [ locus_code, interactors_l2[0], interactors_l3[0], interactors_l4  ]
                        elsif genes_to_match.include?(interactors_l3[0])
                            my_networks << [ locus_code, interactors_l2[0], interactors_l3[0] ]
                        elsif genes_to_match.include?(interactors_l2[0])
                            my_networks << [ locus_code, interactors_l2[0] ]
                        end
                    end
                end
            end
        end
        unique = self.Simplify_list(my_networks)
        if unique.any? then unique.each {|each_one| @@all_networks["#{locus_code}"].associated_networks << each_one} end
        if unique.any? then unique.each {|each_one| @@all_associated_networks << each_one} end
    end

    # Simplify an array of arrays
    # @param [Array] my_networks an array of arrays including all regulatory genes that have been found (or anything else, really)
    # @return [Array] unique a simplified array
    # @note This can be used for any array of arays with format [[],[],[]], no need for it to hold strings or even locus codes! :D
    def self.Simplify_list(my_networks)
        unique = my_networks.uniq
        unique.each() do |first_network|
            unique.each() do |second_network|
                set1 = first_network.to_set
                set2 = second_network.to_set
                if set2.subset?(set1) && first_network.any? && second_network.any? && first_network.length > second_network.length
                    unique.delete(second_network)
                elsif set2.subset?(set1) && first_network.any? && second_network.any? && first_network.length < second_network.length
                    unique.delete(first_network)
                end
            end
        end
        unique.each_with_index{|each, i| unique[i] = each.chunk(&:itself).map(&:first)}
        return unique
    end

    # Returns a Hash of all InteractionNetwork.rb objects
    # @return [Hash] hash of all InteractionNetwork objects, with locus codes as key and InteractionNetwork objects as values
    def self.all_networks()
        return @@all_networks
    end

    # Returns a given InteractionNetwork object when asked for an ID
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @return [Object] Returns a InteractionNetwork object with an ID and a series of linked genes
    def self.find_by_id(locus_code)
        return @@all_networks["#{locus_code}"]
    end

    # Returns a list of all found associated networks; essentially, a hash of all the InteractionNetwork.associated_networks values for each object
    # @return [Hash] returns a hash of a simplified list of all the associated networks
    def self.all_associated_networks()
        return self.Simplify_list(@@all_associated_networks)
    end

end
