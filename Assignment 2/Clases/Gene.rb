#!/usr/bin/env ruby

require './Clases/ImportExportModule.rb'

# @author Pablo Marcos
# A class that, given a certain gene, gets its first-level interactions.
# It has the possibility of adding all regulatory networks the gene takes part in
class Gene

    #Contains the AGI code for each gene
    # @return [String] a string which includes the correct AGI locus code for a gene
    attr_accessor :gene_id
    #Contains all linked genes for a given gene
    # @return [Array] an Array of all genes linked to gene_id, according to EBI-Intact
    attr_accessor :linked_genes
    #Contains all regulatory networks a gene is associated with
    # @return [Array]  an Array of all the networks this gene takes part in
    attr_accessor :networks

    #A Hash of all Gene.rb objects
    @@all_genes = {}

    #A constructor for Gene.rb objects
    def initialize(params = {})
        @gene_id = params.fetch(:gene_id, nil)
        @linked_genes = params.fetch(:linked_genes, [])
        @networks = params.fetch(:networks, [])
    end

    # Finds interactions for a given locus code
    # @param [String] locus_code a locus code for a given gene in which we wish to find interactions
    # @return [Object] a Gene object with an ID and a series of linked genes
    # @note The following filtering choices were made: * the locus code must follow an AGI format, or it will be rejected * mi-score should be higher than 0.485 (a high-average value to keep unreliable interactions out) * both genes must be of Arabidopsis Thaliana (taxa id: 3702)
    def self.find_interactions(locus_code)
        interact_with_locus_code = Array.new
        locus_code = locus_code.upcase
        fetched_data = ImportExportModule.fetch("http://www.ebi.ac.uk/Tools/webservices/psicquic/intact/webservices/current/search/interactor/#{locus_code}")
        fetched_data.split("\n").each do |record|
            genes = record.scan(/(A[Tt]\d[Gg]\d\d\d\d\d)/)
            mi_score = record.match(/i\w+-\w+:(0\.\d+?)/)
            if genes[1] && genes[1][0].upcase != locus_code && mi_score[1].to_f > 0.485 && record.split("\t")[9].include?("3702") && record.split("\t")[10].include?("3702")
                interact_with_locus_code.append("#{genes[1][0].upcase}")
            elsif genes[0][0].upcase != locus_code && mi_score[1].to_f > 0.485 && record.split("\t")[9].include?("3702") && record.split("\t")[10].include?("3702")
                interact_with_locus_code.append("#{genes[0][0].upcase}")
            end
        end
        @@all_genes["#{locus_code}"] = Gene.new(:gene_id => locus_code, :linked_genes => interact_with_locus_code)
        return @@all_genes["#{locus_code}"]
    end

    # Returns a Hash of all Gene.rb objects
    # @return [Hash] Hash of all gene objects, with locus codes as key and gene objects as values
    def self.all_genes()
        return @@all_genes
    end

    # Finds a gene object by it's gene_id property
    # @param [String] locus_code A locus_code for a given gene
    # @return [Object] Returns a Gene object with an ID and a series of linked genes
    def self.find_by_id(locus_code)
        return @@all_genes["#{locus_code}"]
    end

end


