#!/usr/bin/env ruby

require 'json'
require './Clases/Gene.rb'
require './Clases/ImportExportModule.rb'
require './Clases/InteractionNetwork.rb'

# @author Pablo Marcos
#This intends to be the ‘uso-general’ object for Extra Point 1: an object that can hold any funtional anotation
class Annotations

    #Contains the AGI code for each gene
    # @return [String] a string which includes the correct AGI locus code for a gene
    attr_accessor :gene_id
    #Contains its equivalent Gene object, generated if Gene.rb has run
    # @return [Object] an object of class Gene.rb
    attr_accessor :gene_object
    #Contains its equivalent InteractionNetwork object, generated if InteractionNetwork.rb has run
    # @return [Object] an object of class InteractionNetwork.rb
    attr_accessor :interactions_object
    #Contains all Kegg Pathways a gene id is related to
    # @return [Hash]  a Hash of all the Kegg Pathways this gene takes part in, where keys are Kegg:ID and values are Kegg:Terms
    attr_accessor :kegg_terms
    #Contains all Gene Ontologies a gene id is related to
    # @return [Hash]  a Hash of all the GOs this gene takes part in, where keys are GO:ID and values are GO:Terms
    attr_accessor :go_terms

    #A Hash of all Annotations.rb objects
    @@all_anotations = {}

    #A constructor for Annotations.rb objects
    # @note It tries to detect if there are existing Gene.rb or interactions.rb objects and, if there are, it attaches them to the Annotations object
    def initialize(params = {})
        @gene_id = params.fetch(:gene_id, nil)
        @gene_object = Gene.find_by_id(@gene_id)
            unless @gene_object.is_a?(Gene) then @gene_object = nil end
        @interactions_object = InteractionNetwork.find_by_id(@interactions_object)
            unless @interactions_object.is_a?(InteractionNetwork) then @interactions_object = nil end
        @kegg_terms = params.fetch(:kegg_terms, Hash.new)
        @go_terms = params.fetch(:go_terms, Hash.new)

    end

    # Finds Gene Ontologies for a given locus code
    # @param [String] locus_code A locus code for a given gene in which we wish to find Gene Ontologies
    # @return [Hash] a Hash of all the GOs this gene takes part in, where keys are GO:ID and values are GO:Terms
    def self.Annotate_Go(locus_code)
        go_terms = {}
        fetched_data =  ImportExportModule.fetch("http://togows.dbcls.jp/entry/uniprot/#{locus_code}/dr.json")
        data = JSON.parse(fetched_data.body)[0]
        data["GO"].each do |go|
            if go[1] =~ /P:/ #P stands for "biological Process"
                go_terms["#{go[0]}".delete("GO:")] = go[1].delete("P:") #Ignores IEP
            end
        end
        return go_terms
    end

    # Finds Kegg Pathways for a given locus code
    # @param [String] locus_code A locus code for a given gene in which we wish to find Gene Ontologies
    # @return [Hash] a Hash of all the KPs this gene takes part in, where keys are Kegg:ID and values are Kegg:Terms
    def self.Annotate_Kegg(locus_code)
        fetched_data = ImportExportModule.fetch("http://togows.org/entry/kegg-genes/ath:#{locus_code}/pathways.json")
        data = JSON.parse(fetched_data.body)[0] #So as not to get a [{}]
        return data
    end

    # Annotates a given locus code
    # @param [String] locus_code A locus code for a given gene in which we wish to find Gene Ontologies
    # @return [Object] a new Annotations.rb object, with all of its values (if everything went ok): go terms, kegg terms, gene id, gene object and interactions object
    def self.Annotate_Gene(locus_code)
        go_terms = self.Annotate_Go(locus_code)
        kegg_terms = self.Annotate_Kegg(locus_code)
        object = Annotations.new(:gene_id => locus_code.upcase, :kegg_terms => kegg_terms, :go_terms => go_terms, :gene_object => locus_code.upcase, :interactions_object => locus_code.upcase)
        p object
        return object
    end
end
