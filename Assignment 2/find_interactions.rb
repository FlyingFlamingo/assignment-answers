#!/usr/bin/env ruby


# ************************* REQUIREMENTS MODULE ************************* #


require './Clases/Gene.rb'
require './Clases/InteractionNetwork.rb'
require './Clases/Annotations.rb'
require './Clases/ImportExportModule.rb'
require 'progress_bar'


# ************************* PRESENTATION ************************* #


#Some computer talking...
puts "Hi! My name is PILI, your virtual assistant for Gene Interaction!"
puts "You may already know me from other assignments as part of the \'Bioinformatics Programming Challenges\' subject at UPM's Masters in Computational Biology. Its nice to see you back!"


# ************************* IMPUT CHECK MODULE ************************* #


puts "First, let me check the input..."
ImportExportModule.Check_input(ARGV)


#************************* NETWORK-GENERATION MODULE ************************* #


puts
puts "Importing Genes and checking for correlations. Please, be patient..."

all_genes = ImportExportModule.load_table(ARGV[0]) #Load the (presumably existing) SubNetwork_GeneList.txt file

bar = ProgressBar.new(all_genes.length, :bar, :percentage, :elapsed) #Cool! A progress bar
all_genes.each() do |locus_code|
    InteractionNetwork.Build_network(locus_code) #Finds interactions
    InteractionNetwork.Search_in_networks(locus_code, all_genes) #And builds networks for each gene
    bar.increment!
end

puts puts
puts "Genes imported and analysed! Now, lets write a report..."
puts


#************************* REPORT - WRITING MODULE ************************* #


#Gets the networks list and writes a report
interactions_list = InteractionNetwork.all_associated_networks()
ImportExportModule.Write_report(ARGV[1], interactions_list)

#More computer talk...
puts
puts "The script has correctly finalized! See you later for assignment 3!"
